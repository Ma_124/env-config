## Environment
- `SERVER_TIMEOUT=0`: Timeout in milliseconds\.
- `VERBOSITY_LEVEL=warning`: The verbosity level\.


## Verbosity
- `info`: Log informational, warning, and error messages\.
- `warning`: Log warning and error messages\.
- `error`: Log error messages\.