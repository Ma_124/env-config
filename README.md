# `env-config`
Read configuration from the environment and generate documentation (Man and Markdown) for it using [`doc-writer`](https://docs.rs/doc-writer/latest/doc_writer/trait.DocumentationWriter.html).
For usage information and examples see [`docs.rs`](https://docs.rs/env-config).

## License
`env-config` is distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See the [LICENSE-APACHE](./LICENSE-APACHE) and [LICENSE-MIT](./LICENSE-MIT) files in this repository for more information.